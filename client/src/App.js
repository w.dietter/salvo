/* react hot loader */
import 'react-hot-loader';
import { hot } from 'react-hot-loader/root';
import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import 'App.css';
/* components  */
import Navbar from 'components/Navbar';
import GamesPage from 'components/pages/GamesPage';
import GameViewPage from 'components/pages/GameViewPage';

function App() {
	return (
		<Router>
			{/* <Navbar /> */}
			<Switch>
				<Route exact path='/game_view' component={GameViewPage} />
				<Route exact path='' component={GamesPage} />
			</Switch>
			<ToastContainer autoClose={2000} />
		</Router>
	);
}

export default hot(App);
