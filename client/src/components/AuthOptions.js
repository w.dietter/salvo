import React from 'react';
import styled from 'styled-components';
import LoginForm from 'components/LoginForm';
import LogoutBtn from 'components/LogoutBtn';
import { colors } from 'elements';

const AuthWrapper = styled.div`
	width: 100%;
	background: ${colors.primary[1]};
	padding: 1rem 0;
	display: flex;
	justify-content: space-around;
	align-items: center;
`;
const AuthOptions = ({ user, logout }) => {
	const login = () => {
		window.location.reload();
	};

	return (
		<AuthWrapper>
			<div>
				<span className='mr-3'>{user && <LogoutBtn logout={logout} />}</span>
				<span className='text-light'>{user && user.email}</span>
			</div>
			<LoginForm login={login} />
		</AuthWrapper>
	);
};

export default AuthOptions;
