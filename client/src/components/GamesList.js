import React from 'react';
import axios from 'axios';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import GamesListItem from 'components/GamesListItem';
import { Container, Row, Col, Table, Button } from 'reactstrap';
import { colors, MainButton } from 'elements';

const GamesListContainer = styled(Container)`
	width: 100%;
	background-image: url('../images/battleship.jpg');
	background-position: center;
	background-origin: center;
	background-repeat: no-repeat;
	background-size: cover;
	position: relative;

	&::before {
		content: '';
		width: 100%;
		height: 100%;
		position: absolute;
		top: 0;
		left: 0;
		background: ${colors.secondary[5]};
		opacity: 0.1;
	}
`;
const GamesListButton = styled(MainButton)`
	margin: 1rem 0;
`;
const GamesListTable = styled(Table)`
	margin: 1rem 0;
	text-align: center;
	box-shadow: 1px 2px 7px 2px rgba(0, 0, 0, 0.1);
	color: white;
	border-radius: 10px;
	background: rgba(255, 255, 255, 0.95);
`;
const GamesListThead = styled.thead`
	border-bottom: 10px solid ${colors.primary[3]};
	color: inherit;
`;
const GamesListTbody = styled.tbody``;
const GamesList = props => {
	const { games, isLoaded, user, history } = props;
	const createGame = async () => {
		try {
			const res = await axios.post(`/api/games`);
			if (res.status === 201) {
				history.push(`/game_view?gp=${res.data.gpid}`);
			}
		} catch (err) {}
	};
	console.log(games);
	return (
		<GamesListContainer fluid>
			<Row>
				<Col md={2} className='offset-md-5'>
					<GamesListButton
						onClick={createGame}
						bgColor='primary'
						bgShape='3'
						className='text-center w-100'>
						Create Game
					</GamesListButton>
				</Col>
			</Row>
			<Row>
				<Col md={8} className='offset-md-2'>
					<GamesListTable borderless hover>
						<GamesListThead>
							<tr>
								<th>Game</th>
								<th>Creation Date</th>
								<th>Player</th>
								<th>Player</th>
								<th></th>
							</tr>
						</GamesListThead>
						<GamesListTbody>
							{isLoaded &&
								games
									.sort((a, b) => (a.id > b.id ? -1 : 1))
									.map(game => {
										return (
											<GamesListItem key={game.id} game={game} user={user} />
										);
									})}
						</GamesListTbody>
					</GamesListTable>
				</Col>
			</Row>
		</GamesListContainer>
	);
};

export default withRouter(GamesList);
