import React from 'react';
import moment from 'moment';
import axios from 'axios';
import { Link, withRouter } from 'react-router-dom';
import { MainButton } from 'elements';

const GamesListItem = ({ game, user, history }) => {
	const { id, created, gamePlayers } = game;
	const joinGame = async gameId => {
		try {
			const res = await axios.post(`/api/game/${gameId}/players`);
			if (res.status === 201) {
				history.push(`/game_view?gp=${res.data.gpid}`);
			}
		} catch (err) {}
	};
	if (gamePlayers.length < 2) {
		return (
			<tr>
				<td>{id}</td>
				<td>{moment().from(new Date(created), true)}</td>
				{user && gamePlayers[0].name === user.email ? (
					<td>
						<Link to={`/game_view?gp=${gamePlayers[0].gpid}`}>
							{gamePlayers[0].name}
						</Link>
					</td>
				) : (
					<td>{gamePlayers[0].name}</td>
				)}
				<td></td>
				{user && gamePlayers[0].name !== user.email ? (
					<td>
						<MainButton color='success' onClick={() => joinGame(id)}>
							JOIN GAME
						</MainButton>
					</td>
				) : null}
			</tr>
		);
	} else {
		return (
			<tr>
				<td>{id}</td>
				<td>{moment().from(new Date(created), true)}</td>
				{gamePlayers.map(gp => {
					return user && gp.name === user.email ? (
						<td key={gp.id}>
							<Link to={`/game_view?gp=${gp.gpid}`}>{gp.name}</Link>
						</td>
					) : (
						<td key={gp.id}>{gp.name}</td>
					);
				})}
			</tr>
		);
	}
};

export default withRouter(GamesListItem);
