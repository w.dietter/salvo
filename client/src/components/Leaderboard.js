import React from 'react';
import { Table } from 'reactstrap';

const Leaderboard = props => {
	const { players, isLoaded } = props;
	return (
		<>
			<h2 className='text-center'>Leaderboard</h2>
			<Table>
				<thead>
					<tr>
						<th>Player</th>
						<th>Total score</th>
						<th>Wins</th>
						<th>Looses</th>
						<th>Ties</th>
					</tr>
				</thead>
				<tbody>
					{isLoaded &&
						players.map(player => {
							return <LeaderboardRow key={player.id} player={player} />;
						})}
				</tbody>
			</Table>
		</>
	);
};

const LeaderboardRow = ({ player: { name, score, wins, looses, ties } }) => (
	<tr>
		<td>{name}</td>
		<td>{score}</td>
		<td>{wins}</td>
		<td>{looses}</td>
		<td>{ties}</td>
	</tr>
);

export default Leaderboard;
