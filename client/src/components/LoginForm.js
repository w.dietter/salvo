import React from 'react';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import { Form, FormGroup, Input, Col } from 'reactstrap';
/* elements */
import { MainButton } from 'elements';

const INITIAL_STATE = {
	username: '',
	password: ''
};

class LoginForm extends React.Component {
	state = {
		...INITIAL_STATE
	};

	handleChange = ({ target: { value, name } }) => {
		this.setState({
			[name]: value
		});
	};

	login = async e => {
		e.preventDefault();
		try {
			const res = await axios.post(
				'/api/login',
				new URLSearchParams({ ...this.state })
			);
			console.log(res);
			this.props.login();
		} catch (err) {
			console.log(err);
		}
	};

	register = async e => {
		e.preventDefault();
		try {
			const res = await axios.post(
				'/api/players',
				new URLSearchParams({ ...this.state })
			);
			console.log(res);
		} catch (err) {
			console.log(err);
		}
	};

	render() {
		const { username, password } = this.state;
		return (
			<Form className='d-flex align-items-center'>
				<FormGroup row className='m-0'>
					<Col md={4}>
						<Input
							type='text'
							name='username'
							value={username}
							placeholder='username'
							onChange={this.handleChange}
						/>
					</Col>
					<Col md={4}>
						<Input
							type='password'
							name='password'
							value={password}
							placeholder='password'
							onChange={this.handleChange}
						/>
					</Col>
					<Col md={4}>
						<MainButton
							onClick={this.login}
							className='mx-3'
							bgColor='primary'
							bgShape='1'
							textColor='white'
							textShape='1'>
							Login
						</MainButton>
						<MainButton
							onClick={this.register}
							bgColor='primary'
							bgShape='1'
							textColor='white'
							textShape='1'>
							Register
						</MainButton>
					</Col>
				</FormGroup>
			</Form>
		);
	}
}

export default withRouter(LoginForm);
