import React from 'react';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import { MainButton } from 'elements';

const LogoutBtn = ({ logout }) => {
	const logoutFromAPI = async () => {
		const res = await axios.get('/api/logout');
		if (res.status === 200 || res.status === 304) {
			logout();
		}
	};
	return (
		<MainButton
			bgColor='secondary'
			bgShape='5'
			textColor='white'
			textShape='1'
			onClick={logoutFromAPI}>
			Logout
		</MainButton>
	);
};

export default withRouter(LogoutBtn);
