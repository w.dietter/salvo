import React from 'react';
import styled from 'styled-components';
import { colors } from 'elements';

const Nav = styled.nav`
	background: ${props =>
		props.bgColor ? colors[props.bgColor][props.bgShape] : 'white'};
	height: 10vh;
`;

const Navbar = () => {
	return (
		<Nav bgColor='primary' bgShape='1' className='m-0 p-0'>
			<h1 className='m-0 text-center text-light display-3 w-100'>Salvo</h1>
		</Nav>
	);
};

export default Navbar;
