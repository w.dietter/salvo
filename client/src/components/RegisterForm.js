import React from 'react';
import axios from 'axios';
import { Form, FormGroup, Label, Input, Button, Col } from 'reactstrap';

const INITIAL_STATE = {
	username: '',
	password: ''
};

class RegisterForm extends React.Component {
	state = {
		...INITIAL_STATE
	};

	handleChange = ({ target: { value, name } }) => {
		this.setState({
			[name]: value
		});
	};

	handleSubmit = async e => {
	};

	render() {
		const { username, password } = this.state;
		return (
			<Form onSubmit={this.handleSubmit}>
				<FormGroup row>
					<Label for='username' md={3}>
						Username
					</Label>
					<Col md={5}>
						<Input
							type='text'
							name='username'
							value={username}
							onChange={this.handleChange}
						/>
					</Col>
				</FormGroup>
				<FormGroup row>
					<Label for='password' md={3}>
						Password
					</Label>
					<Col md={5}>
						<Input
							type='password'
							name='password'
							value={password}
							onChange={this.handleChange}
						/>
					</Col>
				</FormGroup>
				<Button color='info' type='submit'>
					Register
				</Button>
			</Form>
		);
	}
}

export default RegisterForm;
