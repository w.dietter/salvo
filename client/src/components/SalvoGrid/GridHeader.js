import React from 'react';
import PropTypes from 'prop-types';
import { GridHeaderWrapper, GridHeaderStyle } from 'elements';

const GridHeader = props => {
	const { cols } = props;
	return (
		<GridHeaderStyle>
			<GridHeaderCell key={0} />
			{cols.map(col => (
				<GridHeaderCell key={col} text={col}>
					<span>{col}</span>
				</GridHeaderCell>
			))}
		</GridHeaderStyle>
	);
};

export const GridHeaderCell = props => {
	return <GridHeaderWrapper>{props.text}</GridHeaderWrapper>;
};

GridHeader.propTypes = {
	cols: PropTypes.array.isRequired
};

export default GridHeader;
