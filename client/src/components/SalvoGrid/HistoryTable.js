import React from 'react';
import styled, { keyframes } from 'styled-components';
import { colors } from 'elements';

const enterLeft = keyframes`
	0% {
		transform: translate(-10px, 0);
	}
	100% {
		transform: translate(0,0);
	}
`;

const enterRight = keyframes`
	0% {
		transform: translate(10px, 0);
	}
	100% {
		transform: translate(0,0);
	}
`;

const HistoryTableWrapper = styled.table`
	width: 100%;
	text-align: center;
	font-size: 0.9rem;
	padding: 0.5rem;
	/* background-color: ${colors.primary[1]}; */
	color: white;
`;
const HistoryTableThead = styled.thead`
	font-size: 1rem;
	border-bottom: 3px solid #eee;
	padding: 1rem 0;

	th {
		padding: 0 0.25rem;
	}
`;
const HistoryTableRow = styled.tr`
	border-bottom: 2px dotted ${colors.secondary[2]};
	padding: 0;
	animation: 200ms ${props => (props.id % 2 === 0 ? enterLeft : enterRight)}
		ease;
`;
const HistoryTableCell = styled.td`
	background-color: ${props =>
		props.header ? colors.primary[6] : 'transparent'};
	padding: 0.25rem;
	border-radius: 2px;
`;
const HistoryTablePlayer = styled.div`
	width: 100%;
	text-align: center;
	padding: 1rem 0;
	text-transform: uppercase;
`;

const HistoryTable = props => {
	const { turns, player } = props;
	if (turns && turns.length > 0) {
		turns.sort((a, b) => a.turn - b.turn);
	}
	return (
		<>
			<HistoryTablePlayer>{player}</HistoryTablePlayer>
			{turns && turns.length > 0 && (
				<HistoryTableWrapper>
					<HistoryTableThead>
						<tr>
							<th>Turn</th>
							<th>Hits</th>
							<th>missed</th>
						</tr>
					</HistoryTableThead>
					<tbody>
						{turns.map(t => (
							<HistoryTableRow key={t.turn} id={t.turn}>
								<HistoryTableCell header>{t.turn}</HistoryTableCell>
								<HistoryTableCell>
									{Object.keys(t.damages).map(d => {
										if (d.includes('Hits') && t.damages[d] > 0) {
											return (
												<div className='my-2 text-left' key={d}>
													{d.split('H')[0] + ' ' + t.damages[d]}
												</div>
											);
										}
										return null;
									})}
								</HistoryTableCell>
								<HistoryTableCell>{t.missed}</HistoryTableCell>
							</HistoryTableRow>
						))}
					</tbody>
				</HistoryTableWrapper>
			)}
		</>
	);
};

export default HistoryTable;
