import React from 'react';
import PropTypes from 'prop-types';
import GridHeader, { GridHeaderCell } from 'components/SalvoGrid/GridHeader';
import Salvo from 'components/SalvoGrid/Salvo';
import { SalvoGridContainer } from 'elements';
import SalvoGridCell from 'components/SalvoGrid/SalvoGridCell';
import { ItemTypes } from 'types';

const OponnetsSalvoGrid = ({ cols, rows, grid, toggleSalvo }) => {
	return (
		<>
			<GridHeader cols={cols} />
			<SalvoGridContainer>
				{grid &&
					grid.map((row, i) => {
						return (
							<React.Fragment key={i}>
								<GridHeaderCell text={rows[i]} />
								{row.map((col, j) => {
									return (
										<SalvoGridCell
											data={col}
											key={col.id}
											self={false}
											accept={ItemTypes.SALVO}
											toggleSalvo={() => toggleSalvo(i, j)}>
											{col.hasSalvo && (
												<Salvo location={col.id} turn={col.salvoTurn} />
											)}
										</SalvoGridCell>
									);
								})}
							</React.Fragment>
						);
					})}
			</SalvoGridContainer>
		</>
	);
};

OponnetsSalvoGrid.propTypes = {
	cols: PropTypes.array.isRequired,
	rows: PropTypes.array.isRequired,
	grid: PropTypes.array.isRequired,
	salvos: PropTypes.array.isRequired
};

export default OponnetsSalvoGrid;
