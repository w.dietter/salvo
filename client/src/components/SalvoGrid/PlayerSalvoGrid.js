import React from 'react';
import SalvoGridCell from 'components/SalvoGrid/SalvoGridCell';
import Ship from 'components/SalvoGrid/Ship';
import GridHeader from 'components/SalvoGrid/GridHeader';
import { GridHeaderCell } from 'components/SalvoGrid/GridHeader';
import { SalvoGridContainer } from 'elements';
import { ItemTypes } from 'types';

/* ships, locatedShips, updateOnDrag, changeDirection */
const PlayerSalvoGrid = props => {
	const {
		rows,
		cols,
		grid,
		locatedShips,
		checkSpaceOnDrag,
		changeOrientation,
		updateGridDataOnDragEnd,
		gameState
	} = props;
	return (
		<>
			<GridHeader cols={cols} />
			<SalvoGridContainer>
				{grid &&
					grid.map((row, i) => {
						return (
							<React.Fragment key={i}>
								<GridHeaderCell text={rows[i]} />
								{row.map(col => {
									return (
										<SalvoGridCell
											data={col}
											key={col.id}
											self={true}
											accept={ItemTypes.SHIP}
											checkSpaceOnDrag={checkSpaceOnDrag}>
											{locatedShips.map(ls => {
												if (ls.shipLocations[0] === col.id) {
													return (
														<Ship
															data={ls}
															key={ls.id}
															gameState={gameState}
															updateGridDataOnDragEnd={updateGridDataOnDragEnd}
															changeOrientation={() => changeOrientation(ls)}
														/>
													);
												} else return null;
											})}
										</SalvoGridCell>
									);
								})}
							</React.Fragment>
						);
					})}
			</SalvoGridContainer>
		</>
	);
};

export default PlayerSalvoGrid;
