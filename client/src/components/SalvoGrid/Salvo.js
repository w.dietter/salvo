import React from 'react';
import { SalvoWrapper } from 'elements';

const Salvo = ({ location, turn }) => {
	return <SalvoWrapper>{turn}</SalvoWrapper>;
};

export default Salvo;
