import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import PlayerSalvoGrid from 'components/SalvoGrid/PlayerSalvoGrid';
import ShipsContainer from 'components/SalvoGrid/ShipsContainer';
import OponnetsSalvoGrid from 'components/SalvoGrid/OponnetsSalvoGrid';
import withSalvoGridLogic from 'components/SalvoGrid/withSalvoGridLogic';
import HistoryTable from 'components/SalvoGrid/HistoryTable';
import { GameStateTypes } from 'types';
import { MainButton } from 'elements';
import styled from 'styled-components';

const SalvoGridWrapper = styled(Container)`
	height: auto;
	padding: 2rem 0 0 0;
`;
const SalvoGrid = props => {
	const { grid, ships, locatedShips, salvos, turns, gameState } = props;
	return (
		<SalvoGridWrapper fluid>
			<Row>
				<Col xs='12'>
					<ShipsContainer
						ships={ships}
						updateGridDataOnDragEnd={props.updateGridDataOnDragEnd}
						changeOrientation={props.changeOrientation}
						gameState={gameState}
					/>
				</Col>
			</Row>
			<Row>
				<Col xs='4' className='pl-0'>
					<PlayerSalvoGrid
						cols={props.cols}
						rows={props.rows}
						grid={grid}
						locatedShips={locatedShips}
						checkSpaceOnDrag={props.checkSpaceOnDrag}
						changeOrientation={props.changeOrientation}
						updateGridDataOnDragEnd={props.updateGridDataOnDragEnd}
						gameState={gameState}
					/>
					<MainButton
						disabled={gameState !== GameStateTypes.PLACE_SHIPS}
						className='w-100 mt-2'
						onClick={props.postShips}>
						Place Ships
					</MainButton>
				</Col>
				{turns && (
					<>
						<Col xs='auto' className='p-0'>
							<HistoryTable turns={turns['self']} player='self' />
						</Col>
						<Col xs='auto' className='p-0 '>
							<HistoryTable turns={turns['oponnent']} player='oponnent' />
						</Col>
					</>
				)}
				<Col xs='4' className='pr-0'>
					<OponnetsSalvoGrid
						cols={props.cols}
						rows={props.rows}
						grid={grid}
						salvos={salvos}
						toggleSalvo={props.toggleSalvo}
						turn={props.turn}
					/>
					<MainButton
						disabled={gameState !== GameStateTypes.PLAY}
						className='w-100 mt-2'
						onClick={props.postSalvos}>
						Place Salvos
					</MainButton>
				</Col>
			</Row>
		</SalvoGridWrapper>
	);
};

export default withSalvoGridLogic(SalvoGrid);
