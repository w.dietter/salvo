import React from 'react';
import { useDrop } from 'react-dnd';
import { Cell } from 'elements';

const SalvoGridCell = props => {
	const { data, checkSpaceOnDrag, children, toggleSalvo, accept, self } = props;
	const { id, i, j, selfHit, oponnentHit } = data;
	const [collectedProps, drop] = useDrop({
		accept,
		drop(item) {
			return {
				id,
				i,
				j,
				droppedShip: { ...item.draggedShip, i, j }
			};
		},
		canDrop(item, monitor) {
			return checkSpaceOnDrag(item.draggedShip, i, j);
		},
		collect: monitor => ({
			isOver: monitor.isOver({ shallow: true })
		})
	});

	const { isOver } = collectedProps;
	return (
		<Cell
			ref={drop}
			isOver={isOver}
			onClick={accept === 'salvo' ? toggleSalvo : null}
			selfHit={selfHit}
			self={self}
			oponnentHit={oponnentHit}>
			{children}
		</Cell>
	);
};

export default SalvoGridCell;

/* creates the object to be passed to getDropResult() 
				{
					id: string -> this cell id,
					i:number,
					j: number,
					droppedShip: {
						id: string -> ship id,
						type: string -> ship type e.g: "submarine",
						size: string,
						isLocated: boolean,
						orientation: string
					}
				}
			*/
