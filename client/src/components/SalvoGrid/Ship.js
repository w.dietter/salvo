import React from 'react';
import { useDrag } from 'react-dnd';
import PropTypes from 'prop-types';
import { ItemTypes, GameStateTypes } from 'types';
import { ShipContainer } from 'elements';

const Ship = props => {
	const { data, updateGridDataOnDragEnd, changeOrientation, gameState } = props;
	const { id, type, size, isLocated, orientation } = data;
	const [collectedProps, drag] = useDrag({
		item: {
			id: id,
			type: ItemTypes.SHIP,
			draggedShip: {
				size: size,
				type: type,
				id: id,
				isLocated: isLocated,
				orientation
			}
		},
		canDrag() {
			return gameState === GameStateTypes.PLACE_SHIPS;
		},
		end(item, monitor) {
			const droppedData = monitor.getDropResult();
			updateGridDataOnDragEnd(droppedData);
		},
		collect: monitor => ({
			isDragging: monitor.isDragging(),
			didDrop: monitor.didDrop()
		})
	});
	const { isDragging } = collectedProps;
	return (
		<ShipContainer
			ref={drag}
			orientation={orientation}
			isDragging={isDragging}
			isLocated={isLocated}
			/* UPDATES THE STATE  */
			onClick={changeOrientation}
			size={parseInt(size)}
		/>
	);
};

Ship.propTypes = {
	data: PropTypes.object.isRequired,
	updateGridDataOnDragEnd: PropTypes.func.isRequired,
	changeOrientation: PropTypes.func.isRequired,
	gameState: PropTypes.string.isRequired
};

export default Ship;
