import React from 'react';
import { Container } from 'reactstrap';
import PropTypes from 'prop-types';
import Ship from 'components/SalvoGrid/Ship';

const ShipsContainer = props => {
	const {
		ships,
		updateGridDataOnDragEnd,
		changeOrientation,
		gameState
	} = props;
	return (
		<Container className='d-flex'>
			{ships.map(ship => (
				<Ship
					data={ship}
					key={ship.id}
					updateGridDataOnDragEnd={updateGridDataOnDragEnd}
					changeOrientation={() => changeOrientation(ship)}
					gameState={gameState}
				/>
			))}
		</Container>
	);
};

ShipsContainer.propTypes = {
	ships: PropTypes.array.isRequired,
	updateGridDataOnDragEnd: PropTypes.func.isRequired,
	changeOrientation: PropTypes.func.isRequired
};

export default ShipsContainer;
