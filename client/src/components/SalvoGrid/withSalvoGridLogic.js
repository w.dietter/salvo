import React from 'react';
import axios from 'axios';
import shipsData from 'ship.data.js';
import { toast } from 'react-toastify';
import { getSalvosFlatArray } from 'helpers/gamesHelpers';
import {
	getGrid,
	getOrientation,
	getCurrentTurn
} from 'helpers/salvoGridHelpers';
import { GameStateTypes } from 'types';

const rows = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
const cols = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const withSalvoGridLogic = WrappedComponent => {
	return class extends React.Component {
		constructor(props) {
			super(props);
			this.state = {
				ships: [],
				salvos: [],
				locatedSalvos: [],
				turn: 0,
				turns: { self: {}, oponnent: {} },
				isLoaded: false,
				gameState: ''
			};
			console.log(this.state);
			this.gameInterval = null;
		}

		async componentDidMount() {
			this.gameInterval = setInterval(() => {
				this.updateGameState();
			}, 5000);
			await this.fetchInitialGameData();
		}

		componentWillUnmount() {
			clearInterval(this.gameInterval);
		}

		fetchInitialGameData = async () => {
			try {
				// this.setState({ isLoaded: false });
				const res = await axios.get(
					`/api/game_view/${this.props.gamePlayerId}`
				);
				const gameData = res.data;
				this.setState({
					gameData: gameData,
					ships: gameData.ships.length === 0 ? shipsData : [],
					locatedShips: gameData.ships
						? gameData.ships.map((ship, i) => {
								return {
									...ship,
									id: i,
									size: ship.locations.length,
									shipLocations: ship.locations,
									isLocated: true,
									orientation: getOrientation(ship.locations)
								};
						  })
						: [],
					salvos: gameData.salvos
						? getSalvosFlatArray(gameData.salvos, this.props.gamePlayerId)
						: [],
					locatedSalvos: [],
					grid: [
						...getGrid(
							rows,
							cols,
							gameData.ships,
							gameData.salvos,
							this.props.gamePlayerId
						)
					],
					turn: getCurrentTurn(gameData.salvos, this.props.gamePlayerId),
					turns: gameData.hits,
					gameState: gameData.gameState,
					isLoaded: true
				});
			} catch (error) {
				console.log(error);
			}
		};

		// only checks for gameState
		updateGameState = async () => {
			try {
				const res = await axios.get(
					`/api/game_view/${this.props.gamePlayerId}`
				);
				const gameData = res.data;
				// clear interval when turn end depends on player action only
				if (
					gameData.gameState === GameStateTypes.PLACE_SHIPS ||
					gameData.gameState === GameStateTypes.PLAY
				) {
					clearInterval(this.gameInterval);
					this.updateGameData();
				} else {
					console.log(gameData.gameState);
					this.setState({
						gameState: gameData.gameState
					});
				}
			} catch (error) {
				console.log(error);
			}
		};

		updateGameData = async () => {
			try {
				const res = await axios.get(
					`/api/game_view/${this.props.gamePlayerId}`
				);
				const gameData = res.data;
				this.setState({
					salvos: gameData.salvos
						? getSalvosFlatArray(gameData.salvos, this.props.gamePlayerId)
						: [],
					grid: [
						...getGrid(
							rows,
							cols,
							gameData.ships,
							gameData.salvos,
							this.props.gamePlayerId
						)
					],
					turn: getCurrentTurn(gameData.salvos, this.props.gamePlayerId),
					turns: gameData.hits,
					gameState: gameData.gameState,
					isLoaded: true
				});
				console.log('game data updated');
			} catch (error) {
				console.log(error);
			}
		};

		postShips = async () => {
			const { gamePlayerId } = this.props;
			const { locatedShips } = this.state;
			if (locatedShips.length < 5) {
				toast.error('all ships must be placed');
			}
			const data = locatedShips.map(ship => {
				return {
					type: ship.type,
					locations: ship.shipLocations
				};
			});
			try {
				const res = await axios.post(
					`api/games/players/${gamePlayerId}/ships`,
					data
				);
				console.log(res);
				this.gameInterval = setInterval(() => {
					this.updateGameState();
				}, 5000);
				this.updateGameData();
			} catch (err) {
				console.log(err);
			}
		};

		postSalvos = async () => {
			const { gamePlayerId } = this.props;
			const data = this.state.locatedSalvos.reduce((acc, item, i) => {
				if (i === 0) {
					return {
						turn: item.turn,
						locations: [item.location]
					};
				}
				return {
					turn: acc.turn,
					locations: [...acc.locations, item.location]
				};
			}, {});
			console.log(data);
			try {
				const res = await axios.post(
					`api/games/players/${gamePlayerId}/salvos`,
					data
				);
				this.setState({
					locatedSalvos: []
				});
				this.gameInterval = setInterval(() => {
					this.updateGameState();
				}, 5000);
				this.updateGameData();
				console.log(res);
			} catch (err) {
				console.log(err);
			}
		};

		toggleSalvo = (i, j) => {
			if (this.hasLocationSalvo(i, j)) {
				if (this.state.grid[i][j].salvoTurn === this.state.turn) {
					this.removeSalvoFromGrid(i, j);
				}
			} else {
				this.placeSalvoOnGrid(i, j);
			}
		};

		removeSalvoFromGrid = (i, j) => {
			if (this.state.locatedSalvos.length === 0) return;
			let newGrid = [...this.state.grid];
			newGrid[i][j] = {
				...newGrid[i][j],
				hasSalvo: false,
				salvoTurn: null
			};
			this.setState({
				grid: newGrid,
				locatedSalvos: this.state.locatedSalvos.filter(
					salvo => salvo.location !== newGrid[i][j].id
				)
			});
		};

		placeSalvoOnGrid = (i, j) => {
			if (this.state.locatedSalvos.length === 5) return;
			let newGrid = [...this.state.grid];
			newGrid[i][j] = {
				...newGrid[i][j],
				hasSalvo: true,
				salvoTurn: this.state.turn
			};
			this.setState({
				grid: newGrid,
				locatedSalvos: [
					...this.state.locatedSalvos,
					{ turn: this.state.turn, location: newGrid[i][j].id }
				]
			});
		};

		hasLocationSalvo = (i, j) => {
			return this.state.grid[i][j].hasSalvo;
		};

		/* checkSpaceOnDrag(draggedShip: {
                        size: string,
                        type: string,
                        id: number,
                        isLocated: boolean},
                        iIndex: number,
                        jIndex: number)
    iIndex and jIndex are the coordinates of the grid cell that is being dragged Over
    Check for available space in a drag operation    */

		checkSpaceOnDrag = (draggedShip, iIndex, jIndex) => {
			const { grid } = this.state;
			if (draggedShip.orientation === 'horizontal') {
				if (grid[iIndex][jIndex + parseInt(draggedShip.size) - 1]) {
					for (let offset = 0; offset < draggedShip.size; offset++) {
						if (
							grid[iIndex][jIndex + offset].hasShip &&
							grid[iIndex][jIndex + offset].shipId !== draggedShip.id
						) {
							return false;
						}
					}
					return true;
				}
				return false;
			} else {
				if (grid[iIndex + parseInt(draggedShip.size) - 1]) {
					for (let offset = 0; offset < draggedShip.size; offset++) {
						if (
							grid[iIndex + offset][jIndex].hasShip &&
							grid[iIndex + offset][jIndex].shipId !== draggedShip.id
						) {
							return false;
						}
					}
					return true;
				}
				return false;
			}
		};

		/**
    |--------------------------------------------------
      updateGridDataOnDragEnd():void
      updates the state with a new salvo grid whit the new ship locations, depends of the
      updateGridWithShipLocations function that returns a new grid matrix with the updated
      grid cells.
  
      droppedData: {
        id: string -> grid cell id: e.g "A1", "C3",
        i: number, -> i index of the logic grid
        j: number ->  j index of the logic grid where the drag op has ended.
        droppedShip: {
          draggedShip,
          i,
          j
        }
      }
    |--------------------------------------------------
    */
		updateGridDataOnDragEnd = droppedData => {
			if (!droppedData) return;
			const { i, j, droppedShip } = droppedData || {};
			const { grid, ships, locatedShips } = this.state;
			const shipLocations = [];
			for (let offset = 0; offset < droppedShip.size; offset++) {
				if (droppedShip.orientation === 'horizontal') {
					shipLocations.push(grid[i][j + offset].id);
				} else {
					shipLocations.push(grid[i + offset][j].id);
				}
			}
			if (!droppedShip.isLocated) {
				this.setState({
					grid: this.updateGridWithShipLocations(shipLocations, droppedShip.id),
					ships: ships.filter(ship => ship.id !== droppedShip.id),
					locatedShips: [
						...locatedShips,
						{ ...droppedShip, isLocated: true, shipLocations }
					]
				});
			} else {
				this.setState({
					grid: this.updateGridWithShipLocations(shipLocations, droppedShip.id),
					locatedShips: locatedShips.map(ls => {
						return ls.type === droppedShip.type
							? {
									...droppedShip,
									shipLocations
							  }
							: ls;
					})
				});
			}
		};

		/**
    |--------------------------------------------------
       updateGridWhitShipLocations(locations: string[], shipId: number): grid[][]
       performs a update of the ship locations for a specific ship,
       adds the new locations and clears the old ones.
       THIS FN DOESNT UPDATE THE STATE.
    |--------------------------------------------------
    */
		updateGridWithShipLocations = (locations, shipId) => {
			const { grid } = this.state;
			return grid.map(row => {
				return row.map(col => {
					if (locations.includes(col.id)) {
						return {
							...col,
							hasShip: true,
							shipId
						};
					} else if (col.shipId === shipId && !locations.includes(col.id)) {
						return {
							...col,
							hasShip: false,
							shipId: null
						};
					} else {
						return col;
					}
				});
			});
		};

		/* ORIENTATION CHANGE on Click -> Can be performed when the ship is placed and when is 
    not yet placed, if the ship is placed the locatedShips array must be upated. */

		/**
    |--------------------------------------------------
      checkSpaceOnOrientationChange= (ship: size: string, id:number, ...): boolean
      Check if there is available space to perform an orientation change.
      The available space must be a valid grid cell with hasShip === false 
    |--------------------------------------------------
    */
		checkSpaceOnOrientationChange = ship => {
			if (!ship) return;
			const { i: iIndex, j: jIndex } = ship;
			const { grid } = this.state;
			if (ship.orientation === 'vertical') {
				if (grid[iIndex][jIndex + parseInt(ship.size) - 1]) {
					for (let offset = 1; offset < ship.size; offset++) {
						if (grid[iIndex][jIndex + offset].hasShip) {
							return false;
						}
					}
					return true;
				}
				return false;
			} else {
				if (grid[iIndex + parseInt(ship.size) - 1]) {
					for (let offset = 1; offset < ship.size; offset++) {
						if (grid[iIndex + offset][jIndex].hasShip) {
							return false;
						}
					}
					return true;
				}
				return false;
			}
		};

		/**
    |--------------------------------------------------
      changeOrientation(ship):void
      UPDATES THE STATE
      Performs a orientation change of the ship and updates the state
      If the ship is not located there is no state to update.
      If the ship is located the state is updated with a new grid matrix.
    |--------------------------------------------------
    */
		changeOrientation = ship => {
			const { ships, locatedShips } = this.state;
			if (!ship.isLocated) {
				this.setState({
					ships: ships.map(s => {
						if (s.id === ship.id) {
							return s.orientation === 'horizontal'
								? { ...s, orientation: 'vertical' }
								: { ...s, orientation: 'horizontal' };
						}
						return s;
					})
				});
			} else if (ship.isLocated && this.checkSpaceOnOrientationChange(ship)) {
				const newShipLocations = this.getShipLocationsOnOrientationChange(ship);
				this.setState({
					grid: this.updateGridWithShipLocations(newShipLocations, ship.id),
					locatedShips: locatedShips.map(ls => {
						if (ls.id === ship.id) {
							return ls.orientation === 'horizontal'
								? {
										...ls,
										orientation: 'vertical',
										shipLocations: newShipLocations
								  }
								: {
										...ls,
										orientation: 'horizontal',
										shipLocations: newShipLocations
								  };
						}
						return ls;
					})
				});
			} else {
				alert('not enough space!');
				return;
			}
		};

		getShipLocationsOnOrientationChange = ship => {
			if (!ship) return;
			const { grid } = this.state;
			const { i, j } = ship;
			const shipLocations = [];
			for (let offset = 0; offset < ship.size; offset++) {
				// is reversed
				if (ship.orientation === 'horizontal') {
					shipLocations.push(grid[i + offset][j].id);
				} else {
					shipLocations.push(grid[i][j + offset].id);
				}
			}
			return shipLocations;
		};

		render() {
			return (
				<WrappedComponent
					{...this.props}
					{...this.state}
					rows={rows}
					cols={cols}
					changeOrientation={this.changeOrientation}
					checkSpaceOnDrag={this.checkSpaceOnDrag}
					checkSpaceOnOrientationChange={this.checkSpaceOnOrientationChange}
					getShipLocationsOnOrientationChange={
						this.getShipLocationsOnOrientationChange
					}
					postShips={this.postShips}
					postSalvos={this.postSalvos}
					updateGridDataOnDragEnd={this.updateGridDataOnDragEnd}
					updateGridWithShipLocations={this.updateGridWithShipLocations}
					toggleSalvo={this.toggleSalvo}
				/>
			);
		}
	};
};

export default withSalvoGridLogic;
