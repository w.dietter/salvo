import React from 'react';
import axios from 'axios';
import SalvoGrid from 'components/SalvoGrid/SalvoGrid';
import { Container } from 'reactstrap';
import { DndProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import styled from 'styled-components';
import { colors, shadows } from 'elements';

const GameViewWrapper = styled(Container)`
	background-image: url('../images/abstract.jpg');
	background-size: cover;
	background-position: center;
	padding: 0 !important;
	box-shadow: ${shadows('lg')};
`;

const PlayerBadge = styled.div`
	/* background: ${colors.primary[5]}; */
	padding: 1rem;
	display: flex;
	justify-content: space-between;
	align-items: center;
	height: 15vh;
	h3 {
		font-size: 1.5rem;
		padding: 0.5rem;
		/* background: ${colors.white[2]}; */
		border-radius: 5px;
		border: 3px solid ${colors.primary[1]};
		box-shadow: ${shadows('sm')};
	}
	h4 {
		font-size: 1.1rem;
		padding: 0.5rem;
		/* background: ${colors.white[2]}; */
		border-radius: 5px;
		border: 3px solid ${colors.primary[1]};
		box-shadow: ${shadows('sm')};
	}
`;

class GameViewPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoaded: false
		};
	}

	async componentDidMount() {
		await this.fetchGameViewData();
	}

	fetchGameViewData = async () => {
		const search = new URLSearchParams(this.props.location.search);
		const gamePlayer = search.get('gp');
		try {
			const res = await axios.get(`/api/game_view/${gamePlayer}`);
			const { gamePlayers = [], gameState } = res.data;
			const { email } = gamePlayers.filter(
				gp => gp.id === parseInt(gamePlayer)
			)[0].player;
			this.setState({
				isLoaded: true,
				playerName: email,
				gamePlayerId: gamePlayer,
				gameState
			});
		} catch (err) {
			console.log(err);
			if (err.response) {
				if (err.response.status === 401) {
					this.props.history.push('/login');
				}
			}
			console.log(err);
		}
	};

	render() {
		console.log(this.state);
		const { playerName, isLoaded, gamePlayerId, gameState } = this.state;
		return (
			<GameViewWrapper fluid className='w-100 '>
				{isLoaded ? (
					<>
						<PlayerBadge>
							<h3>{playerName}</h3>
							<h4>{gameState}</h4>
						</PlayerBadge>
						<DndProvider backend={HTML5Backend}>
							<SalvoGrid gamePlayerId={gamePlayerId} />
						</DndProvider>
					</>
				) : (
					<h1>Loading...</h1>
				)}
			</GameViewWrapper>
		);
	}
}

export default GameViewPage;
