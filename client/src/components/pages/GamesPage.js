import React from 'react';
import axios from 'axios';
import {
	sortGamesByDate,
	getPlayersLeaderboardData
} from 'helpers/gamesHelpers';
import { Container, Row } from 'reactstrap';
/* Component */
import Leaderboard from 'components/Leaderboard';
import GamesList from 'components/GamesList';
import AuthOptions from 'components/AuthOptions';

class GamesPage extends React.Component {
	state = {
		games: [],
		user: '',
		players: [],
		isLoaded: false
	};

	async componentDidMount() {
		try {
			const res = await axios.get('/api/games');
			let { games, player } = res.data;
			games = sortGamesByDate(games);
			const players = getPlayersLeaderboardData(games);
			this.setState({
				games,
				user: player,
				players,
				isLoaded: true
			});
		} catch (err) {
			console.log(err.response);
		}
	}

	logout = () => {
		this.setState({ user: null });
		this.forceUpdate();
	};

	render() {
		const { players, isLoaded, user, games } = this.state;
		console.log(user);
		return (
			<Container className='mt-0' fluid={true}>
				<Row>
					<AuthOptions user={user} logout={this.logout} />
				</Row>
				<Row>
					<GamesList user={user} isLoaded={isLoaded} games={games} />
				</Row>
				<Row>{/* <Leaderboard players={players} isLoaded={isLoaded} /> */}</Row>
			</Container>
		);
	}
}

export default GamesPage;
