import styled from 'styled-components';
import { colors } from 'elements';
import { Button } from 'reactstrap';

export const MainButton = styled(Button)`
	background: ${props =>
		props.bgColor && props.bgShape
			? colors[props.bgColor][props.bgShape]
			: colors.primary[3]} !important;
	color: ${props =>
		props.textColor && props.textShape
			? colors[props.textColor][props.textShape]
			: colors.white[2]} !important;
	border: 2px solid
		${props =>
			props.textColor
				? colors[props.textColor[props.textShape]]
				: 'white'} !important;
	box-shadow: 2px 5px 5px 2px rgba(0, 0, 0, 0.1);
`;
