import styled from 'styled-components';
import { colors } from './colors';

export const SalvoGridContainer = styled.div`
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
`;

export const GridHeaderStyle = styled.div`
	display: flex;
	flex-direction: row;
`;

export const GridHeaderWrapper = styled.div`
	flex: 1 1 9%;
	background: ${colors.secondary[1]};
	border: 1px solid #333;
	min-height: 2rem;
	display: flex;
	justify-content: center;
	align-items: center;
	color: white;
`;

export const Cell = styled.div`
	flex: 1 1 9%;
	position: relative;
	width: 7vw;
	height: 6vh;
	/* border: 1px solid #eee; */
	/* box-shadow: 0px 0px 3px 1px #333 inset; */
	text-align: center;
	background-color: ${({ isOver }) => (isOver ? 'gold' : colors.white[0])};
	transition: background-color 200ms ease-in;
	display: flex;
	justify-content: center;
	align-items: center;
	box-shadow: ${({ self, selfHit, oponnentHit }) =>
		hitType(selfHit, oponnentHit, self)};
`;

const hitType = (selfHit, oponnentHit, self) => {
	if (selfHit && self) {
		return '1px 1px 10px 3px #fff';
	}
	if (oponnentHit && !self) {
		return '0px 0px 2px 2px red';
	}
	return '0px 0px 3px 1px #333 inset';
};
