import styled from 'styled-components';

export const SalvoWrapper = styled.div`
	width: 100%;
	height: 100%;
	position: absolute;
	top: 0;
	left: 0;
	background: tomato;
`;
