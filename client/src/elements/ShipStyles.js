import styled, { css, keyframes } from 'styled-components';

const colors = ['tomato', 'teal', 'lightgreen', 'purple', 'lime', 'orange'];

export const ShipContainer = styled.div`
	box-sizing: border-box;
	${props => (props.isLocated ? located : notLocated)}
	visibility: ${props => (props.isDragging ? 'hidden' : 'visible')};
  background: ${props => colors[props.size]};
`;

const placeShip = keyframes`
	from {
		opacity: 0;
		transform: scale(0);
		/* transform: translateX(10px); */
	}
	to {
		opacity: 1;
		transform: scale(1);
		/* transform: translateX(0); */
	}
`;
export const located = css`
	position: absolute;
	top: 0;
	left: 0;
	z-index: 1;
	display: flex;
	width: ${props => props.size * 100 + '%'};
	height: 100%;
	/* width: 100%;
	height: 100%; */
	/* animation: ${placeShip} 200ms linear 1; */
	/* &::before {
		content: '';
		position: absolute;
		background: ${props => colors[props.size]};
		z-index: 1;
		${props =>
			props.orientation === 'horizontal'
				? css`
						width: ${props => (props.size - 1) * 100 + '%'};
						height: 100%;
						top: 0;
						left: 100%;
				  `
				: css`
						width: 100%;
						height: ${props => (props.size - 1) * 100 + '%'};
						top: 100%;
						left: 0%;
				  `};
	} */
`;

export const notLocated = css`
	margin: 1rem;
	${props =>
		props.orientation === 'horizontal'
			? css`
					width: ${props => props.size * 25 + 'px'};
					height: 50px;
			  `
			: css`
					width: 50px;
					height: ${props => props.size * 25 + 'px'};
			  `}
`;
