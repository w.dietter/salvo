export const colors = {
	primary: [
		'#866fdd',
		'#795fd9',
		'#6c4fd6',
		'#6348c3',
		'#5941b0',
		'#4f3a9c',
		'#453389'
	],
	secondary: [
		'#8ec6eb',
		'#7EBEE8',
		'#6EB6E5',
		'#5EAEE2',
		'#4EA6E0',
		'#4797CC',
		'#4088B8'
	],
	white: ['#F2F7FF', '#F2F7FF', '#F2F7FF'],
	black: ['#343533', '#20211F', '#151514']
};
