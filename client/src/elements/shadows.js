export const shadows = size => {
	if (size === 'sm') {
		return '1px 2px 5px 1px rgba(0,0,0,0.2)';
	}
	if (size === 'lg') {
		return '10px 48px 79px -24px rgba(0,0,0,0.55)';
	}
	return '0px 0px 8px 1px rgba(0,0,0,0.55)';
};
