export const sortGamesByDate = games => {
	games.sort((a, b) => {
		return a.created < b.created ? -1 : 1;
	});
	return games;
};

export const getPlayersLeaderboardData = games => {
	let players = [];
	games.forEach(game => {
		game.gamePlayers.forEach(({ id, name, score: gpScore }) => {
			const { score } = gpScore || {};
			let pi = players.findIndex(p => p.id === id);
			if (pi !== -1) {
				if (score >= 0) {
					players[pi].wins =
						score === 1 ? players[pi].wins + 1 : players[pi].wins;
					players[pi].ties =
						score === 0.5 ? players[pi].ties + 1 : players[pi].ties;
					players[pi].losses =
						score === 0 ? players[pi].losses + 1 : players[pi].losses;
					players[pi].score = score
						? players[pi].score + score
						: players[pi].score;
				}
			} else {
				let playerData = {
					id,
					name,
					wins: score === 1 ? 1 : 0,
					ties: score === 0.5 ? 1 : 0,
					losses: score === 0 ? 1 : 0,
					score: score || 0
				};
				players.push(playerData);
			}
		});
	});
	players.sort((a, b) => {
		if (a.score > b.score) return -1;
		else if (a.score < b.score) return 1;
		else return 0;
	});
	players.sort((a, b) => {
		if (a.losses < b.losses) return -1;
		else if (a.losses > b.losses) return 1;
		else return 0;
	});
	return players;
};

export const getSalvosFlatArray = (salvos, gamePlayerId) => {
	let newSalvos = salvos.filter(
		salvo => salvo.player === parseInt(gamePlayerId)
	);
	newSalvos = newSalvos
		.map(s => {
			return s.locations.map(l => {
				return {
					turn: s.turn,
					location: l
				};
			});
		})
		.flat();
	return newSalvos;
};
