import { getSalvosFlatArray } from './gamesHelpers';

export const getGrid = (rows, cols, ships, salvos, gamePlayerId) => {
	let newGrid = rows.map((row, i) => {
		return cols.map((col, j) => {
			return {
				id: `${row}${col}`,
				hasSalvo: false,
				hasShip: false,
				i,
				j
			};
		});
	});

	if (ships.length > 0) {
		newGrid = populateGridWithShips(
			newGrid,
			ships.map((ship, i) => {
				return {
					...ship,
					id: i,
					size: ship.locations.length,
					shipLocations: ship.locations,
					isLocated: true,
					orientation: getOrientation(ship.location)
				};
			})
		);
	}

	if (salvos.length > 0) {
		newGrid = populateGridWithSalvos(
			newGrid,
			getSalvosFlatArray(salvos, gamePlayerId)
		);
	}

	newGrid = populateHits(newGrid, salvos, gamePlayerId);
	return newGrid;
};

export const populateHits = (grid, salvos, gamePlayerId) => {
	let selfSalvos = salvos.filter(
		salvo => salvo.player === parseInt(gamePlayerId)
	);
	let oponnentSalvos = salvos.filter(
		salvo => salvo.player !== parseInt(gamePlayerId)
	);
	selfSalvos = selfSalvos
		.map(salvo => {
			return salvo.locations.map(location => {
				return {
					turn: salvo.turn,
					location
				};
			});
		})
		.flat();
	oponnentSalvos = oponnentSalvos
		.map(salvo => {
			return salvo.locations.map(location => {
				return {
					turn: salvo.turn,
					location
				};
			});
		})
		.flat();

	return grid.map(row => {
		return row.map(col => {
			return {
				...col,
				selfHit: oponnentSalvos.find(
					salvo => salvo.location === col.id && col.hasShip
				)
					? true
					: false,
				oponnentHit: selfSalvos.find(
					salvo => salvo.location === col.id && col.hasShip
				)
					? true
					: false
			};
		});
	});
};

/* grid: [][], ships: ship[] */
export const populateGridWithShips = (oldGrid, ships) => {
	let grid = [...oldGrid];
	ships.forEach(ship => {
		grid = grid.map((row, i) => {
			return row.map((col, j) => {
				if (!col.hasShip && ship.locations.includes(col.id)) {
					return {
						...col,
						hasShip: true,
						shipId: ship.id
					};
				}
				return col;
			});
		});
	});
	return grid;
};

export const populateGridWithSalvos = (oldGrid, salvos) => {
	return oldGrid.map((row, i) => {
		return row.map((col, j) => {
			const salvo = salvos.find(salvo => salvo.location === col.id);
			return salvo ? { ...col, hasSalvo: true, salvoTurn: salvo.turn } : col;
		});
	});
};

export const getCurrentTurn = (salvos, gamePlayerId) => {
	let turn;
	turn = salvos.reduce((acc, salvo) => {
		return salvo.player === parseInt(gamePlayerId) && salvo.turn > acc
			? salvo.turn
			: acc;
	}, 0);
	return turn + 1;
};

/* locations: string[] */
export const getOrientation = locations => {
	if (locations) {
		if (locations[0][0] !== locations[1][0]) {
			return 'vertical';
		} else {
			return 'horizontal';
		}
	}
};

export const getMatrixIndexes = (grid, locations) => {
	const indexes = {};
	grid.forEach((row, i) => {
		row.forEach((col, j) => {
			if (col.id === locations[0]) {
				indexes.i = i;
				indexes.j = j;
			}
		});
	});
	return indexes;
};
