const shipsData = [
	{
		id: 1,
		type: 'Carrier',
		size: 5,
		orientation: 'horizontal',
		isLocated: false
	},
	{
		id: 2,
		type: 'Battleship',
		size: 4,
		orientation: 'horizontal',
		isLocated: false
	},
	{
		id: 3,
		type: 'Submarine',
		size: 3,
		orientation: 'horizontal',
		isLocated: false
	},
	{
		id: 4,
		type: 'Destroyer',
		size: 3,
		orientation: 'horizontal',
		isLocated: false
	},
	{
		id: 5,
		type: 'Patrol Boat',
		size: 2,
		orientation: 'horizontal',
		isLocated: false
	}
];

export default shipsData;
