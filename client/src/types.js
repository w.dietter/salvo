export const ItemTypes = {
	SHIP: 'ship',
	SALVO: 'salvo'
};

export const GameStateTypes = {
	PLACE_SHIPS: 'PLACE_SHIPS',
	WAIT_FOR_OPONNENT: 'WAIT_FOR_OPONNENT',
	WAIT: 'WAIT',
	PLAY: 'PLAY',
	WIN: 'WIN',
	LOOSE: 'LOOSE',
	TIE: 'TIE'
};
