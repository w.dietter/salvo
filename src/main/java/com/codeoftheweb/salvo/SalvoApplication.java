package com.codeoftheweb.salvo;

import com.codeoftheweb.salvo.models.*;
import com.codeoftheweb.salvo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;


@SpringBootApplication
public class SalvoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SalvoApplication.class, args);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Bean
    public CommandLineRunner initData(
            PlayerRepository playerRepository,
            GameRepository gameRepository,
            GamePlayerRepository gamePlayerRepository,
            ShipRepository shipRepository,
            SalvoRepository salvoRepository,
            ScoreRepository scoreRepository,
            PasswordEncoder passwordEncoder) {
        return (args) -> {

            // Players

            Player bauer = new Player("jbauer@ctu.gov", passwordEncoder.encode(
                    "24"));
            Player obrian = new Player("c.obrian@ctu.gov",
                    passwordEncoder.encode("42"));
            Player kimbauer = new Player("kim_bauer@gmail.com",
                    passwordEncoder.encode("kb"));
            Player almeida = new Player("t.almeida@ctu.gov",
                    passwordEncoder.encode("mole"));
            Player p5 = new Player("diego", passwordEncoder.encode("asdasd"));
            playerRepository.save(p5);

            playerRepository.save(bauer);
            playerRepository.save(obrian);
            playerRepository.save(kimbauer);
            playerRepository.save(almeida);


            LocalDateTime now = LocalDateTime.now();
            Instant instant = now.atZone(ZoneId.systemDefault())
                                 .toInstant();

            //Game Dates
            Date game1Date = Date.from(instant);
            Date game2Date = Date.from(instant.plusSeconds(3600));
            Date game3Date = Date.from(instant.plusSeconds(3600 * 2));
            Date game4Date = Date.from(instant.plusSeconds(3600 * 3));
            Date game5Date = Date.from(instant.plusSeconds(3600 * 4));
            Date game6Date = Date.from(instant.plusSeconds(3600 * 5));
            Date game7Date = Date.from(instant.plusSeconds(3600 * 6));
            Date game8Date = Date.from(instant.plusSeconds(3600 * 7));

            //Finish Game Dates
            Date game1FinishDate = Date.from(instant.plusSeconds(1800));
            Date game2FinishDate = Date.from(instant.plusSeconds(3600 + 1800));
            Date game3FinishDate =
                    Date.from(instant.plusSeconds((3600 * 2) + 1800));
            Date game4FinishDate =
                    Date.from(instant.plusSeconds((3600 * 3) + 1800));

            //Games

            Game g1 = new Game(game1Date);
            Game g2 = new Game(game2Date);
            Game g3 = new Game(game3Date);
            Game g4 = new Game(game4Date);
            Game g5 = new Game(game5Date);
            Game g6 = new Game(game6Date);
            Game g7 = new Game(game7Date);
            Game g8 = new Game(game8Date);

            gameRepository.save(g1);
            gameRepository.save(g2);
            gameRepository.save(g3);
            gameRepository.save(g4);
            gameRepository.save(g5);
            gameRepository.save(g6);
            gameRepository.save(g7);
            gameRepository.save(g8);
            //Scores
            Score scoreGame1Player1 = new Score(g1, bauer, 1, game1FinishDate);
            Score scoreGame1Player2 = new Score(g1, obrian, 0, game1FinishDate);
            Score scoreGame2Player1 = new Score(g2, bauer, (float) 0.5,
                    game2FinishDate);
            Score scoreGame2Player2 = new Score(g2, obrian, (float) 0.5,
                    game2FinishDate);
            Score scoreGame3Player2 = new Score(g3, obrian, 1, game3FinishDate);
            Score scoreGame3Player3 = new Score(g3, kimbauer, 0, game3FinishDate);
            Score scoreGame4Player2 = new Score(g4, obrian, (float) 0.5,
                    game4FinishDate);
            Score scoreGame4Player1 = new Score(g4, bauer, (float) 0.5,
                    game4FinishDate);

            scoreRepository.save(scoreGame1Player1);
            scoreRepository.save(scoreGame1Player2);
            scoreRepository.save(scoreGame2Player1);
            scoreRepository.save(scoreGame2Player2);
            scoreRepository.save(scoreGame3Player2);
            scoreRepository.save(scoreGame3Player3);
            scoreRepository.save(scoreGame4Player2);
            scoreRepository.save(scoreGame4Player1);

            //GamePlayers

            //Game 1
            GamePlayer gp1 = new GamePlayer(g1, bauer, game1Date);
            GamePlayer gp2 = new GamePlayer(g1, obrian, game1Date);
            // Game 2
            GamePlayer gp3 = new GamePlayer(g2, bauer, game2Date);
            GamePlayer gp4 = new GamePlayer(g2, obrian, game2Date);
            // Game 3
            GamePlayer gp5 = new GamePlayer(g3, obrian, game3Date);
            GamePlayer gp6 = new GamePlayer(g3, kimbauer, game3Date);
            //Game 4
            GamePlayer gp7 = new GamePlayer(g4, obrian, game4Date);
            GamePlayer gp8 = new GamePlayer(g4, bauer, game4Date);
            //Game 5
            GamePlayer gp9 = new GamePlayer(g5, kimbauer, game5Date);
            GamePlayer gp10 = new GamePlayer(g5, bauer, game5Date);
            //Game 6
            GamePlayer gp11 = new GamePlayer(g6, almeida, game6Date);
            //Game 7
            GamePlayer gp12 = new GamePlayer(g7, kimbauer, game7Date);
            //Game 8
            GamePlayer gp13 = new GamePlayer(g8, almeida, game8Date);
            GamePlayer gp14 = new GamePlayer(g8, kimbauer, game8Date);

            gamePlayerRepository.save(gp1);
            gamePlayerRepository.save(gp2);
            gamePlayerRepository.save(gp3);
            gamePlayerRepository.save(gp4);
            gamePlayerRepository.save(gp5);
            gamePlayerRepository.save(gp6);
            gamePlayerRepository.save(gp7);
            gamePlayerRepository.save(gp8);
            gamePlayerRepository.save(gp9);
            gamePlayerRepository.save(gp10);
            gamePlayerRepository.save(gp11);
            gamePlayerRepository.save(gp12);
            gamePlayerRepository.save(gp13);
            gamePlayerRepository.save(gp14);

            //Game 1
            Ship s1 = new Ship(gp1, "Destroyer", Arrays.asList("H2", "H3",
                    "H4"));
            Ship s2 = new Ship(gp1, "Submarine", Arrays.asList("E1", "F1",
                    "G1"));
            Ship s3 = new Ship(gp1, "Patrol Boat", Arrays.asList("B4", "B5"));
            Ship s4 = new Ship(gp2, "Destroyer", Arrays.asList("B5", "C5",
                    "D5"));
            Ship s5 = new Ship(gp2, "Patrol Boat", Arrays.asList("F1", "F2"));
            //Game 2
            Ship s6 = new Ship(gp3, "Destroyer", Arrays.asList("B5", "C5",
                    "D5"));
            Ship s7 = new Ship(gp3, "Patrol Boat", Arrays.asList("C6", "C7"));
            Ship s8 = new Ship(gp4, "Submarine", Arrays.asList("A2", "A3",
                    "A4"));
            Ship s9 = new Ship(gp4, "Patrol Boat", Arrays.asList("G6", "H6"));
            //Game 3
            Ship s10 = new Ship(gp5, "Destroyer", Arrays.asList("B5", "C5",
                    "D5"));
            Ship s11 = new Ship(gp5, "Patrol Boat", Arrays.asList("C6", "C7"));
            Ship s12 = new Ship(gp6, "Submarine", Arrays.asList("A2", "A3",
                    "A4"));
            Ship s13 = new Ship(gp6, "Patrol Boat", Arrays.asList("G6", "H6"));
            //Game 4
            Ship s14 = new Ship(gp7, "Destroyer", Arrays.asList("B5", "C5",
                    "D5"));
            Ship s15 = new Ship(gp7, "Patrol Boat", Arrays.asList("C6", "C7"));
            Ship s16 = new Ship(gp8, "Submarine", Arrays.asList("A2", "A3",
                    "A4"));
            Ship s17 = new Ship(gp8, "Patrol Boat", Arrays.asList("G6", "H6"));
            //Game 5
            Ship s18 = new Ship(gp9, "Destroyer", Arrays.asList("B5", "C5",
                    "D5"));
            Ship s19 = new Ship(gp9, "Patrol Boat", Arrays.asList("C6", "C7"));
            Ship s20 = new Ship(gp10, "Submarine", Arrays.asList("A2", "A3",
                    "A4"));
            Ship s21 = new Ship(gp10, "Patrol Boat", Arrays.asList("G6", "H6"));
            //Game 6
            Ship s22 = new Ship(gp11, "Destroyer", Arrays.asList("B5", "C5",
                    "D5"));
            Ship s23 = new Ship(gp11, "Patrol Boat", Arrays.asList("C6", "C7"));
            //Game 7
            //Game 8
            Ship s24 = new Ship(gp13, "Destroyer", Arrays.asList("B5", "C5",
                    "D5"));
            Ship s25 = new Ship(gp13, "Patrol Boat", Arrays.asList("C6", "C7"));
            Ship s26 = new Ship(gp14, "Submarine", Arrays.asList("A2", "A3",
                    "A4"));
            Ship s27 = new Ship(gp14, "Patrol Boat", Arrays.asList("G6", "H6"));

            shipRepository.save(s1);
            shipRepository.save(s2);
            shipRepository.save(s3);
            shipRepository.save(s4);
            shipRepository.save(s5);
            shipRepository.save(s6);
            shipRepository.save(s7);
            shipRepository.save(s8);
            shipRepository.save(s9);
            shipRepository.save(s10);
            shipRepository.save(s11);
            shipRepository.save(s12);
            shipRepository.save(s13);
            shipRepository.save(s14);
            shipRepository.save(s15);
            shipRepository.save(s16);
            shipRepository.save(s17);
            shipRepository.save(s18);
            shipRepository.save(s19);
            shipRepository.save(s20);
            shipRepository.save(s21);
            shipRepository.save(s22);
            shipRepository.save(s23);
            shipRepository.save(s24);
            shipRepository.save(s25);
            shipRepository.save(s26);
            shipRepository.save(s27);

            //Salvos

            //Game 1 -> gp1, gp2
            Salvo sal1 = new Salvo(gp1, 1, Arrays.asList("B5", "C5", "F1"));
            Salvo sal2 = new Salvo(gp2, 1, Arrays.asList("B4", "B5", "B6"));
            Salvo sal3 = new Salvo(gp1, 2, Arrays.asList("F2", "D5"));
            Salvo sal4 = new Salvo(gp2, 2, Arrays.asList("E1", "H3", "A2"));
            //Game 2 -> gp3, gp4
            Salvo sal5 = new Salvo(gp3, 1, Arrays.asList("A2", "A4", "G6"));
            Salvo sal6 = new Salvo(gp4, 1, Arrays.asList("B5", "D5", "C7"));
            Salvo sal7 = new Salvo(gp3, 2, Arrays.asList("A3", "H6"));
            Salvo sal8 = new Salvo(gp4, 2, Arrays.asList("C5", "C6"));
            //Game 3 -> gp5, gp6
            Salvo sal9 = new Salvo(gp5, 1, Arrays.asList("G6", "H6", "A4"));
            Salvo sal10 = new Salvo(gp6, 1, Arrays.asList("H1", "H2", "H3"));
            Salvo sal11 = new Salvo(gp5, 2, Arrays.asList("A2", "A3", "D8"));
            Salvo sal12 = new Salvo(gp6, 2, Arrays.asList("E1", "F2", "G3"));
            //Game 4 -> gp7, gp8
            Salvo sal13 = new Salvo(gp7, 1, Arrays.asList("A3", "A4", "F7"));
            Salvo sal14 = new Salvo(gp8, 1, Arrays.asList("B5", "C6", "H1"));
            Salvo sal15 = new Salvo(gp7, 2, Arrays.asList("A2", "G6", "H6"));
            Salvo sal16 = new Salvo(gp8, 2, Arrays.asList("C5", "C7", "D5"));
            //Game 5 -> gp9, gp10
            Salvo sal17 = new Salvo(gp9, 1, Arrays.asList("A1", "A2", "A3"));
            Salvo sal18 = new Salvo(gp10, 1, Arrays.asList("B5", "B6", "C7"));
            Salvo sal19 = new Salvo(gp9, 2, Arrays.asList("G6", "G7", "G8"));
            Salvo sal20 = new Salvo(gp10, 2, Arrays.asList("C6", "D6", "E6"));
            Salvo sal21 = new Salvo();
            Salvo sal22 = new Salvo(gp10, 3, Arrays.asList("H1", "H8"));

            salvoRepository.save(sal1);
            salvoRepository.save(sal2);
            salvoRepository.save(sal3);
            salvoRepository.save(sal4);
            salvoRepository.save(sal5);
            salvoRepository.save(sal6);
            salvoRepository.save(sal7);
            salvoRepository.save(sal8);
            salvoRepository.save(sal9);
            salvoRepository.save(sal10);
            salvoRepository.save(sal11);
            salvoRepository.save(sal12);
            salvoRepository.save(sal13);
            salvoRepository.save(sal14);
            salvoRepository.save(sal15);
            salvoRepository.save(sal16);
            salvoRepository.save(sal17);
            salvoRepository.save(sal18);
            salvoRepository.save(sal19);
            salvoRepository.save(sal20);
            salvoRepository.save(sal21);
            salvoRepository.save(sal22);

        };
    }
}


@Configuration
class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

    @Autowired
    PlayerRepository playerRepository;

    @Override
    public void init(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(inputName -> {
            Player player = playerRepository.findByUserName(inputName);
            if (player != null) {
                return new User(player.getUserName(),
                        player.getPassword(),
                        AuthorityUtils.createAuthorityList("USER"));
            } else {
                throw new UsernameNotFoundException("Unknown user" +
                        inputName);
            }
        });
    }
}

//Authorization
@Configuration
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/web/games.html","/api/games", "/api/login",
                        "/api/logout", "/api/players", "/h2-console",
                        "/api/game_view/1").permitAll()
                .antMatchers("/api/**").authenticated()
            .and()
                .formLogin()
                .loginProcessingUrl("/api/login");

        http.headers().frameOptions().disable();

        http.formLogin()
            .usernameParameter("username")
            .passwordParameter("password");

        http.csrf()
            .disable();

        http.exceptionHandling()
            .authenticationEntryPoint((req, res, exc)
                    -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

        http.formLogin()
            .successHandler((req, res, auth) ->
                    clearAuthenticationAttributes(req));

        http.formLogin()
            .failureHandler((req, res, exc) -> res.sendError
                    (HttpServletResponse.SC_UNAUTHORIZED));

        http.logout()
            .logoutUrl("/api/logout")
            .logoutSuccessHandler(new
                    HttpStatusReturningLogoutSuccessHandler());

    }

    private void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        }

    }


}