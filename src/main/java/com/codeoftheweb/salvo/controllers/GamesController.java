package com.codeoftheweb.salvo.controllers;

import com.codeoftheweb.salvo.models.Game;
import com.codeoftheweb.salvo.models.GamePlayer;
import com.codeoftheweb.salvo.models.Player;
import com.codeoftheweb.salvo.repositories.GamePlayerRepository;
import com.codeoftheweb.salvo.repositories.GameRepository;
import com.codeoftheweb.salvo.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping(path = "/api")
public class GamesController {

    @Autowired
    GameRepository gameRepository;

    @Autowired
    GamePlayerRepository gamePlayerRepository;

    @Autowired
    PlayerRepository playerRepository;

    @RequestMapping(path = "/games")
    public Map<String, Object> getGames(Authentication authentication) {
        Map<String, Object> dto = new LinkedHashMap<>();
        Player player = getPlayerOfAuthentication(authentication);

        dto.put("player", player != null ? player.playerDTO() : null);
        dto.put("games", gameRepository.findAll()
                .stream()
                .map(game -> game.gameDTO())
                .collect(toList()));
        return dto;
    }

    @RequestMapping(path = "/games", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> createGame(
            Authentication authentication) {

        Player player = getPlayerOfAuthentication(authentication);
        if (player == null) {
            return new ResponseEntity<>(makeMap("msg", "unauthorized"),
                    HttpStatus.UNAUTHORIZED);
        }

        Game game = new Game(new Date());
        gameRepository.save(game);
        GamePlayer gp = new GamePlayer(game, player, new Date());
        Long gpid = gamePlayerRepository.save(gp)
                                        .getId();
        return new ResponseEntity<>(makeMap("gpid", gpid),
                HttpStatus.CREATED);
    }


    public Map<String, Object> makeMap(String key, Object value) {
        Map<String, Object> simpleJson = new LinkedHashMap<>();
        simpleJson.put(key, value);
        return simpleJson;
    }

    public Player getPlayerOfAuthentication(Authentication authentication) {
        if (authentication != null && authentication.getName() != null) {
            return playerRepository.findByUserName(authentication.getName());
        }
        if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            return null;
        }
        return null;
    }
}
