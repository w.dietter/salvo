package com.codeoftheweb.salvo.controllers;

import com.codeoftheweb.salvo.models.Game;
import com.codeoftheweb.salvo.models.GamePlayer;
import com.codeoftheweb.salvo.models.Player;
import com.codeoftheweb.salvo.repositories.GamePlayerRepository;
import com.codeoftheweb.salvo.repositories.GameRepository;
import com.codeoftheweb.salvo.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping(path = "/api")
public class PlayerController {
    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    GameRepository gameRepository;

    @Autowired
    GamePlayerRepository gamePlayerRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    /* ---------- create new player / sign up -----------*/
    @RequestMapping(path = "/players", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> createNewPlayer(
            @RequestParam String username,
            @RequestParam String password) {

        if (username.isEmpty() || password.isEmpty()) {
            return new ResponseEntity<>(makeMap("msg", "Missing Data"),
                    HttpStatus.FORBIDDEN);
        }

        if (playerRepository.findByUserName(username) != null) {

            return new ResponseEntity<>(makeMap("msg", "Name in use"),
                    HttpStatus.CONFLICT);
        }
        Player newPlayer = new Player(username,
                passwordEncoder.encode(password));
        playerRepository.save(newPlayer);
        return new ResponseEntity<>(makeMap("player",
                newPlayer.getUserName()),
                HttpStatus.CREATED);
    }


    /* ------------ join game -----------*/
    @RequestMapping(path = "/game/{gameId}/players", method =
            RequestMethod.POST)
    public ResponseEntity<Object> joinGame(
            @PathVariable Long gameId, Authentication auth) {
        Player player = getPlayerOfAuthentication(auth);
        if (player == null) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Game game = gameRepository.findById(gameId)
                                  .orElse(null);
        if (game == null) {
            return new ResponseEntity<>(makeMap("msg", "No such game"),
                    HttpStatus.FORBIDDEN);
        }
        Set<GamePlayer> gamePlayers = game.getGamePlayers();
        if (gamePlayers.size() > 1) {
            return new ResponseEntity<>(makeMap("msg", "Game is full"),
                    HttpStatus.FORBIDDEN);
        }
        GamePlayer gp = new GamePlayer(game, player, new Date());
        Long gpid = gamePlayerRepository.save(gp)
                                        .getId();
        return new ResponseEntity<>(makeMap("gpid", gpid),
                HttpStatus.CREATED);
    }

    public Map<String, Object> makeMap(String key, Object value) {
        Map<String, Object> simpleJson = new LinkedHashMap<>();
        simpleJson.put(key, value);
        return simpleJson;
    }

    public Player getPlayerOfAuthentication(Authentication authentication) {
        if (authentication != null && authentication.getName() != null) {
            return playerRepository.findByUserName(authentication.getName());
        }
        if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            return null;
        }
        return null;
    }
}
