package com.codeoftheweb.salvo.controllers;

import com.codeoftheweb.salvo.models.*;
import com.codeoftheweb.salvo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@RestController
@RequestMapping(path = "/api")
public class SalvoController {

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private GamePlayerRepository gamePlayerRepository;

    @Autowired
    private SalvoRepository salvoRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private ShipRepository shipRepository;

    /*PLACE SALVOS -----------------------------------------------------*/
    @RequestMapping(path = "/games/players/{gamePlayerId}/salvos", method =
            RequestMethod.POST)
    public ResponseEntity<Object> addSalvos(
            @RequestBody Salvo salvo,
            @PathVariable Long gamePlayerId,
            Authentication auth) {

        Player player = getPlayerOfAuthentication(auth);
        if (player == null) {
            return new ResponseEntity<>(makeMap("msg", "no player with " +
                    "that id"),
                    HttpStatus.UNAUTHORIZED);
        }
        GamePlayer gp =
                gamePlayerRepository.findById(gamePlayerId)
                                    .orElse(null);
        if (gp == null || gp.getPlayer()
                            .getUserName() != player.getUserName()) {
            return new ResponseEntity<>(makeMap("msg", "different gp"),
                    HttpStatus.UNAUTHORIZED);
        }

        if (!gp.getSalvos()
               .stream()
               .allMatch(s -> s.getTurn() < salvo.getTurn())
        ) {
            return new ResponseEntity<>(makeMap("msg", "forbidden"),
                    HttpStatus.FORBIDDEN);
        }

        Salvo newSalvo = new Salvo(gp, salvo.getTurn(),
                salvo.getLocations());
        salvoRepository.save(newSalvo);
        return new ResponseEntity<>(makeMap("msg", "salvos added"),
                HttpStatus.CREATED);
    }

    /* PLACE SHIPS --------------------------------------------------------*/
    @RequestMapping(path = "/games/players/{gamePlayerId}/ships", method =
            RequestMethod.POST)
    public ResponseEntity<Object> placeShips(
            @RequestBody Set<Ship> ships,
            @PathVariable Long gamePlayerId,
            Authentication auth) {
        Player player = getPlayerOfAuthentication(auth);

        if (player == null) {
            return new ResponseEntity<>(makeMap("msg", "unauthorized"),
                    HttpStatus.UNAUTHORIZED);
        }

        GamePlayer gp =
                gamePlayerRepository.findById(gamePlayerId)
                                    .orElse(null);

        if (gp == null || gp.getPlayer()
                            .getUserName() != player.getUserName()) {
            return new ResponseEntity<>(makeMap("msg", "unauthorized"),
                    HttpStatus.UNAUTHORIZED);
        }

        if (gp.getShips()
              .size() > 0) {
            return new ResponseEntity<>(makeMap("msg", "Ships already " +
                    "placed"), HttpStatus.FORBIDDEN);
        }

        for (Ship ship : ships) {
            ship.setGamePlayer(gp);
            shipRepository.save(ship);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /* -------------- GAME VIEW --------------------------------*/
    @RequestMapping(path = "/game_view/{gamePlayerId}")
    public ResponseEntity<Map<String, Object>> getGameView(
            @PathVariable Long gamePlayerId,
            Authentication authentication) {

        if (isGuest(authentication)) {
            return new ResponseEntity<>(makeMap("msg", "unauthorized"),
                    HttpStatus.UNAUTHORIZED);
        }

        Player player = getPlayerOfAuthentication(authentication);
        if (player == null) {
            return new ResponseEntity<>(makeMap("msg", "unauthorized"),
                    HttpStatus.UNAUTHORIZED);
        }

        GamePlayer gamePlayer = gamePlayerRepository
                .findById(gamePlayerId)
                .orElse(null);

        if (gamePlayer == null) {
            return new ResponseEntity<>(makeMap("msg", "unauthorized"),
                    HttpStatus.CONFLICT);
        }

        if (gamePlayer.getPlayer()
                      .getId() != player.getId()) {
            return new ResponseEntity<>(makeMap("msg", "unauthorized"),
                    HttpStatus.CONFLICT);
        }

        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        Set<GamePlayer> gamePlayersOfThisGame = gamePlayer.getGame()
                                                          .getGamePlayers();
        dto.put("id",
                gamePlayer.getGame()
                          .getId());
        dto.put("created",
                gamePlayer.getGame()
                          .getCreationDate()
                          .getTime());
        dto.put("gamePlayers",
                gamePlayer.getGame()
                          .getGamePlayers()
                          .stream()
                          .map(gp -> gp.gamePlayerDTO())
                          .collect(toList()));
        dto.put("ships", gamePlayer
                .getShips()
                .stream()
                .map(Ship::getShipDTO)
                .collect(toList()));
        dto.put("salvos",
                salvoFlatListDTO(gamePlayersOfThisGame));
        dto.put("hits", makeHitsDTO(gamePlayer));
        dto.put("gameState", getGameState(gamePlayer));
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    //**************************************************************************
    /* HELPERS----------------------------------------------------------------*/

    public Map<String, Object> makeHitsDTO(GamePlayer gamePlayer) {
        Map<String, Object> hitsDTO = new LinkedHashMap<>();
        GamePlayer self = gamePlayer;
        GamePlayer oponnent = self.getOponnent();

        if (oponnent != null) {
            hitsDTO.put("self", self.getHitsForGamePlayer(self, oponnent));
            hitsDTO.put("oponnent", oponnent.getHitsForGamePlayer(oponnent,
                    self));
        } else {
            hitsDTO.put("self", null);
            hitsDTO.put("oponnent", null);
        }
        return hitsDTO;
    }

    public GameState getGameState(GamePlayer gamePlayer) {
        Game game = gamePlayer.getGame();
        GamePlayer oponnent = gamePlayer.getOponnent();
        boolean sameTurn;
        boolean gamePlayerTurn;

        if (oponnent == null) {
            return GameState.WAIT_FOR_OPONNENT;
        }

        sameTurn = gamePlayer.getSalvos()
                             .size() == oponnent.getSalvos()
                                                .size();
        gamePlayerTurn =
                gamePlayer.getSalvos()
                          .size() < oponnent.getSalvos()
                                            .size();

        if (gamePlayer.getShips()
                      .size() == 0) {
            return GameState.PLACE_SHIPS;
        }

        if (game.getGamePlayers()
                .size() == 2
        ) {

            if (gamePlayer.getSalvos()
                          .size() > 0
                    && oponnent.getSalvos()
                               .size() > 0) {
                if (sameTurn
                        && !gamePlayer.allShipsSunken()
                        && oponnent.allShipsSunken()
                ) {
                    return GameState.WIN;
                }

                if (sameTurn
                        && gamePlayer.allShipsSunken()
                        && !oponnent.allShipsSunken()) {
                    return GameState.LOOSE;
                }

                if (sameTurn
                        && gamePlayer.allShipsSunken()
                        && oponnent.allShipsSunken()) {
                    return GameState.TIE;
                }
            }

            if (sameTurn && gamePlayer.getId() < oponnent.getId()) {
                return GameState.PLAY;
            }

            if (gamePlayerTurn) {
                return GameState.PLAY;
            }

            if (sameTurn && gamePlayer.getId() > oponnent.getId()) {
                return GameState.WAIT;
            }

            if (!gamePlayerTurn) {
                return GameState.WAIT;
            }
        }
        return GameState.WAIT;
    }

    public static Set<Map<String, Object>> setOfTurnAndLocationsDTO(
            List<Salvo> salvos) {
        return salvos.stream()
                     .map(s -> s.salvoTurnAndLocationsDTO())
                     .collect(Collectors.toSet());
    }

    private List<? extends Object> salvoFlatListDTO(
            Set<GamePlayer> gamePlayers) {
        List<Map<String, Object>> flatListSalvos = new ArrayList<>();
        for (GamePlayer gp : gamePlayers) {
            flatListSalvos.addAll(gp.getSalvos()
                                    .stream()
                                    .map(s -> s.salvoFlatListDTO())
                                    .collect(toSet()));
        }
        return flatListSalvos;
    }

    public Map<String, Object> makeMap(String key, Object value) {
        Map<String, Object> simpleJson = new LinkedHashMap<>();
        simpleJson.put(key, value);
        return simpleJson;
    }

    public Player getPlayerOfAuthentication(Authentication authentication) {
        if (authentication != null && authentication.getName() != null) {
            return playerRepository.findByUserName(authentication.getName());
        }
        if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            return null;
        }
        return null;
    }

    private boolean isGuest(Authentication authentication) {
        return authentication == null || authentication instanceof AnonymousAuthenticationToken;
    }
}
