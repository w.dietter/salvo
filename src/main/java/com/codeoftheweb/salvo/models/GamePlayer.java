package com.codeoftheweb.salvo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.*;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Entity
public class GamePlayer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "game_id")
    private Game game;

    @ManyToOne
    @JoinColumn(name = "player_id")
    private Player player;

    @OneToMany(mappedBy = "gamePlayer", fetch = FetchType.EAGER)
    private Set<Ship> ships = new HashSet<>();

    @OneToMany(mappedBy = "gamePlayer", fetch = FetchType.EAGER)
    private List<Salvo> salvos = new ArrayList<>();

    private Date joinDate;

    public GamePlayer() {
    }

    public GamePlayer(Game game, Player player, Date joinDate) {
        this.game = game;
        this.player = player;
        this.joinDate = joinDate;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Set<Ship> getShips() {
        return ships;
    }

    public void setShips(Set<Ship> ships) {
        this.ships = ships;
    }

    public List<Salvo> getSalvos() {
        return salvos;
    }

    public Map<String, Object> gamePlayerDTO() {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", this.getId());
        dto.put("player", this.getPlayer()
                .playerDTO());
        return dto;
    }

    public Map<String, Object> gamesWithPlayersDTO() {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("gpid", this.getId());
        dto.put("id", this.getPlayer().getId());
        dto.put("name", this.getPlayer().getUserName());
        dto.put("score", this.getScore() != null ?
                this.getScore().scoreDTO() : null);
        return dto;
    }

    public Score getScore() {
        Score score = this.getPlayer()
                .getScore(this.getGame());
        if (score != null) {
            return score;
        }
        return null;
    }

    public GamePlayer getOponnent() {
        return this.getGame().getGamePlayers()
                .stream()
                .filter(gp -> gp.getId() != this.getId())
                .findFirst()
                .orElse(null);
    }

    public List<Map<String, Object>> getHitsForGamePlayer(GamePlayer self, GamePlayer oponnent) {
        List<Map<String, Object>> gamePlayerHits = new ArrayList<Map<String, Object>>();
        Map<String, Object> damageAccumulator = new LinkedHashMap<>();
        Map<String, Object> shipHitsDTO;
        Map<String, Object> damages = new LinkedHashMap<>();
        List<String> hits;
        int totalHits;
        int hitsAcc;
        Set<Ship> selfShips = self.getShips();
        List<Salvo> oponnentSalvos = oponnent.getSalvos()
                .stream()
                .sorted((a, b) -> a.getTurn() - b.getTurn())
                .collect(toList());

        for (Ship ship : selfShips) {
            damageAccumulator.put(ship.getType(), 0);
        }

        for (Salvo salvo : oponnentSalvos) {
            shipHitsDTO = new LinkedHashMap<>();
            totalHits = 0;
            shipHitsDTO.put("turn", salvo.getTurn());
            for (Ship ship : selfShips) {
                // get location hits for this ship
                hits = ship.getHits(salvo);
                //number of hits for this ship in this turn
                damages.put(ship.getType() + "Hits", hits.size());
                hitsAcc = (int) damageAccumulator.get(ship.getType());

                damages.put(ship.getType(), hitsAcc + hits.size());
                // update hit accumulator for this ship
                damageAccumulator.put(ship.getType(), hitsAcc + hits.size());


                totalHits += hits.size();
            }

            // calculate missed hits for this turn
            shipHitsDTO.put("damages", damages);
            shipHitsDTO.put("missed", salvo.getLocations().size() - totalHits);
            gamePlayerHits.add(shipHitsDTO);
        }
        return gamePlayerHits;
    }

    public boolean allShipsSunken() {
        GamePlayer oponnent = this.getOponnent();
        List<String> salvos =
                oponnent.getSalvos()
                        .stream()
                        .map(salvo -> salvo.getLocations())
                        .flatMap(Collection::stream)
                        .collect(toList());
        return this.getShips()
                .stream()
                .map(ship -> ship.getLocations())
                .flatMap(Collection::stream)
                .allMatch(salvos::contains);
    }
}
