package com.codeoftheweb.salvo.models;

public enum GameState {
    PLACE_SHIPS,
    WAIT_FOR_OPONNENT,
    WAIT,
    PLAY,
    WIN,
    LOOSE,
    TIE
}
