package com.codeoftheweb.salvo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.*;

@Entity
public class Salvo {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
  @GenericGenerator(name = "native", strategy = "native")
  private Long id;

  @ManyToOne
  @JoinColumn(name = "gamePlayer_id")
  private GamePlayer gamePlayer;

  private int turn;

  @ElementCollection
  @Column(name = "location")
  List<String> locations = new ArrayList<>();

  public Salvo() {}

  public Salvo (GamePlayer gamePlayer, int turn, List<String> locations) {
    this.gamePlayer = gamePlayer;
    this.turn = turn;
    this.locations = locations;
  }

  public Map<String, Object> salvoFlatListDTO() {
    Map <String, Object> dto = new LinkedHashMap<>();
    dto.put("turn", this.getTurn());
    dto.put("player", this.getGamePlayer().getId());
    dto.put("locations", this.getLocations());
    return dto;
  }

  public Map<String, Object> salvoTurnAndLocationsDTO() {
    Map <String, Object> dto = new LinkedHashMap<>();
    dto.put(String.valueOf(this.getTurn()), this.getLocations());
    return dto;
  }

  public Long getId() {
    return id;
  }

  public GamePlayer getGamePlayer() {
    return gamePlayer;
  }

  public int getTurn() {
    return turn;
  }

  public List<String> getLocations() {
    return locations;
  }

  public void setGamePlayer(GamePlayer gamePlayer) {
    this.gamePlayer = gamePlayer;
  }

  public void setTurn(int turn) {
    this.turn = turn;
  }

  public void setLocations(List<String> locations) {
    this.locations = locations;
  }

  @Override
  public String toString() {
    return "Salvo{" +
      "id=" + id +
      ", gamePlayer=" + gamePlayer +
      ", turn=" + turn +
      ", locations=" + locations +
      '}';
  }
}
