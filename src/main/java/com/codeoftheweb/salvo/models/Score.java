package com.codeoftheweb.salvo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@Entity
public class Score {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "game_id")
    private Game game;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "player_id")
    private Player player;

    private float score;
    private Date finishDate;

    public Score() {}

    public Score(Game game, Player player, float score, Date finishDate) {
        this.game = game;
        this.player = player;
        this.score = score;
        this.finishDate = finishDate;
    }

    public Map<String, Object> simpleScoreDTO() {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("player", this.getPlayer().simplePlayerDTO());
        dto.put("game", this.getGame().simpleGameDTO());
        dto.put("finishDate", this.getFinishDate());
        dto.put("score", this.score);
        return dto;
    }
    public Map<String, Object> scoreDTO() {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("score", this.getScore());
        dto.put("finishDate", this.getFinishDate().getTime());
        return dto;
    }

    public Long getId() {
        return id;
    }

    public Game getGame() {
        return game;
    }

    public Player getPlayer() {
        return player;
    }

    public float getScore() {
        return score;
    }

    public Date getFinishDate() {
        return finishDate;
    }


}
