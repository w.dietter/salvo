/* 
Salvo Grid

td
atrributes:
	-	data-cell -> is a grid cell from A1 to J10
	- data-ship -> true/false -> if true the cell has a ship
	- data-header -> td with no data, row headers -> A, B, C...
	- data-salvo -> true/false -> the cell has salvo
  - data-hit -> true/false -> the cell has a ship and a salvo
  
*/

class SalvoGrid {
	constructor(target, ships, salvos, rows, columns) {
		this.rows = rows || ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
		this.columns = columns || [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
		this.grid = this.generateGrid();
		this.gridHTML = this.generateSalvoGridHTML();
		this.target = target || '';
		this.ships = ships || [];
		this.salvos = salvos || [];
	}

	generateGrid() {
		return this.rows.map(row => {
			return this.columns.map(col => {
				return col !== 0
					? {
							id: `${row}${col}`
					  }
					: {
							id: `${row}`
					  };
			});
		});
	}

	generateSalvoGridHTML() {
		const newGrid = this.grid.reduce((acc, col) => {
			const htmlCol = col.reduce((acc, row, i) => {
				if (i === 0) {
					return (acc += `<td class="row-head" data-header="true">${row.id}</td>`);
				}
				return (acc += `
				<td id="${row.id}" 
					data-ship="false"
					data-cell="${row.id}" 
					data-salvo="false"
					data-hit="false"
				>
				</td>`);
			}, '');
			acc += `<tr>${htmlCol}</tr>`;
			return acc;
		}, '');
		return newGrid;
	}
	/* The grid is a list of table rows and needs to be mounted on a tbody tag. */
	mountHTMLGridOnTBody(tbodyTarget = this.target) {
		this.target = tbodyTarget;
		document.querySelector(tbodyTarget).innerHTML = this.gridHTML;
	}

	setShips(ships) {
		this.ships = ships;
	}

	/* helpers */
	getArrayOfAllCells() {
		console.log(this.target);
		return Array.from(
			document.querySelectorAll(`${this.target} tr td[data-cell]`)
		);
	}

	setShipsLocation(ships = this.ships) {
		console.log(ships);
		this.getArrayOfAllCells().forEach(cell => {
			if (ships.includes(cell.dataset.cell)) {
				cell.dataset.ship = 'true';
				cell.classList.add('has-ship');
			}
		});
	}

	setSalvosLocation(salvos = this.salvos) {
		console.log(salvos);
		this.getArrayOfAllCells().forEach(cell => {
			salvos.forEach(salvosByTurn => {
				if (salvosByTurn.locations.includes(cell.dataset.cell)) {
					if (cell.dataset.ship === 'true') {
						cell.dataset.salvo = 'true';
						cell.dataset.hit = 'true';
						cell.classList.add('has-hit');
						cell.innerText = salvosByTurn.turn;
					} else {
						cell.dataset.salvo = 'true';
						cell.classList.add('has-salvo');
						cell.innerText = salvosByTurn.turn;
					}
				}
			});
		});
	}
}
