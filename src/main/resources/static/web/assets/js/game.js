const urlParams = new URLSearchParams(window.location.search);

/* view functions */
const setGameTitle = (id, gamePlayers = [], target) => {
	const titleNode = document.querySelector(target);
	const playerOne = gamePlayers[0] ? gamePlayers[0].player.email : '';
	const playerTwo = gamePlayers[1] ? gamePlayers[1].player.email : '';
	titleNode.innerText = `Game ${id} ${playerOne} vs ${playerTwo}`;
};

const setPlayerInfo = (player, target) => {
	const gamePlayerInfoNode = document.querySelector(target);
	gamePlayerInfoNode.innerText = `Player point of view: ${player.email}`;
};

/* helpers */
const getPlayer = (gamePlayers = [], gamePlayerId) => {
	const [gamePlayer] = gamePlayers.filter(gp => gp.id == gamePlayerId);
	const { player } = gamePlayer || {};
	console.log('Player viewing:' + player.email);
	return player;
};

const getOpponent = (gamePlayers = [], gamePlayerId) => {
	const [gamePlayer] = gamePlayers.filter(gp => gp.id != gamePlayerId);
	const { player } = gamePlayer || {};
	return player;
};

/* helpers */
const getFlatArrayOfShipLocations = ships => {
	return ships
		.map(ship => {
			return ship.locations;
		})
		.flat();
};

/*  Main function  */
const getGameView = async gamePlayerId => {
	/* get data from api */
	try {
		const response = await fetch(
			`http://localhost:8080/api/game_view/${gamePlayerId}`
		);
		if (response.status === 200) {
			/* Prepare data */
			const {
				gamePlayers,
				ships,
				salvosFlatList: salvos,
				id
			} = await response.json();
			// console.log(ships);

			const player = getPlayer(gamePlayers, gamePlayerId);
			const playerSalvos = salvos.filter(salvo => salvo.player == player.id);
			const opponent = getOpponent(gamePlayers, gamePlayerId);
			const opponentSalvos = salvos.filter(
				salvo => salvo.player == opponent.id
			);

			/* Player Info */
			setGameTitle(id, gamePlayers, '#gameTitle');
			setPlayerInfo(player, '#playerInfo');

			/* create Grid */

			/* Player */
			const playerSalvoGrid = new SalvoGrid('#player-tbody');
			playerSalvoGrid.mountHTMLGridOnTBody();
			playerSalvoGrid.setShipsLocation(getFlatArrayOfShipLocations(ships));
			playerSalvoGrid.setSalvosLocation(opponentSalvos);

			/* Opponent */
			const opponentSalvoGrid = new SalvoGrid('#opponent-tbody');
			opponentSalvoGrid.mountHTMLGridOnTBody();
			opponentSalvoGrid.setSalvosLocation(playerSalvos);
		}

		if (response.status === 401) {
			const data = await response.json();
			alert('Unauthorized');
			window.location.replace('/web/games.html');
		}
	} catch (error) {
		console.log(error);
	}
};


/* main */
const logoutBtn = document.querySelector('#logoutBtn');
const placeShipsBtn = document.querySelector('#placeShipsBtn');

getGameView(urlParams.get('gp'));

logoutBtn.addEventListener('click', async () => {
	await logout();
	window.location.replace('http://localhost:8080/web/games.html');
});

placeShipsBtn.addEventListener('click', async () => {
		try {
			const response = await fetch(`http://localhost:8080/api/games/players/${urlParams.get('gp')}/ships`,
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify([
					{type: "destroyer", locations: ["A1", "B1", "C1"]},
					{type: "patrol boat", locations: [ "H5", "H6"	]}
				])	
			});
			console.log(response);
			const data = await response.json();
			console.log(data);
		} catch(error) {
			console.log(error);
		}
});

