const login = async (username, password) => {
	const body = {
		username,
		password
	};
	try {
		const response = await fetch('http://localhost:8080/api/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			body: new URLSearchParams(body)
		});
		if (response.status === 200) {
			console.log('logged in!');
			return window.location.reload();
		}
		return response.json();
	} catch (error) {
		throw new Error(error.message);
	}
};

const logout = async reload => {
	try {
		const response = await fetch('http://localhost:8080/api/logout');
		if (response.status === 200) {
			console.log('logged out!');
			if (reload) {
				window.location.reload();
			}
		}
	} catch (err) {
		console.log(err);
	}
};
